//  latian 1
function tampilkan(){
    console.log('HALO');
}
 tampilkan();

//  latian 2
function munculAngkaDua(){
    return 2;
}
var temp = munculAngkaDua();
console.log(temp);

// latian 3
function kaliDua(angka){
    return angka * 2;
}
var temp = kaliDua(4);
console.log(temp);

// latian 4
function tampilangka(angka1, angka2){
    return angka1 + angka2;
}
var temp = tampilangka(5,2);
console.log(temp);

// latian 5
function nilaiDefault(angka = 5){
    return angka;
}
console.log(nilaiDefault());
console.log(nilaiDefault(21));

// latian 6

var fungsiKali = function perkalian(angka1, angka2){
    return angka1 * angka2;
}
console.log(fungsiKali(21,21));

// latian
function introduce(nama, umur){
    return "Halo nama saya " + nama + " umur " + umur;
}
var name = "joni";
var age = 21;

var perkenalan = introduce(name, age);
console.log(perkenalan)