// Soal 1
var word = 'Javascript ';
var second = 'is ';
var third = 'awesome ';
var fourth = 'and ';
var fifth = 'i ';
var sixth = 'love ';
var seventh = 'it';

console.log(word.concat(second, third, fourth, fifth, sixth, seventh));
// Soal 2
var sentences = "I am Going to be React Native Developer";
var exampleFirstWord = sentences[0];
var exampleSecondWord = sentences[2] + sentences[3];
var ThirdWord = sentences[5] + sentences[6] + sentences[7] + sentences[8] + sentences[9];
var FourthWord = sentences[11] + sentences[12];
var FifthWord = sentences[14] + sentences[15];
var SixthWord = sentences[17] + sentences[18] + sentences[19] + sentences[20] + sentences[21];
var SeventhWord = sentences[23] + sentences[24] + sentences[25] + sentences[26] + sentences[27] + sentences[28];
var EigthWord = sentences[30] + sentences[31] + sentences[32] + sentences[33] + sentences[34] + sentences[35] + sentences[36] + sentences[37] + sentences[38];

console.log("First Word: " + exampleFirstWord);
console.log("Second Word :" + exampleSecondWord);
console.log("Third Word :" + ThirdWord);
console.log("Fourth Word :" + FourthWord);
console.log("Fifth Word :" + FifthWord);
console.log("Sixth Word :" + SixthWord);
console.log("Fourth Word :" + SeventhWord);
console.log("Fourth Word :" + EigthWord);

// Soal 3
var sentences2 = "wow Javasript is so cool";

var exampleFirstWord2 = sentences2.substring(0, 3);
var secondword = sentences2.substring(4, 13);
var thirdword = sentences2.substring(14, 16);
var fourthword = sentences2.substring(17, 19);
var fifthword = sentences2.substring(20, 24);


console.log("First Word :" + exampleFirstWord2);
console.log("Second Word :" + secondword);
console.log("Third Word :" + thirdword);
console.log("Fourth Word :" + fourthword);
console.log("Fifth Word :" + fifthword);

// Soal 4
var sentences3 = "wow Javasript is so cool";

var exampleFirstWord3 = sentences3.substring(0, 3);
var secondword3 = sentences3.substring(4, 13);
var thirdword3 = sentences3.substring(14, 16);
var fourthword3 = sentences3.substring(17, 19);
var fifthword3 = sentences3.substring(20, 24);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondword3.length;
var thirdWordLength = thirdword3.length;
var fourthWordLength = fourthword3.length;
var fifthWordLength = fifthword3.length;

console.log("First Word :" + exampleFirstWord3 + ", with length :" + firstWordLength);
console.log("Second Word :" + secondword3 + ", with length :" + secondWordLength);
console.log("Third Word :" + thirdword3 + ", with length :" + thirdWordLength);
console.log("Fourth Word :" + fourthword3 + ", with length :" +fourthWordLength);
console.log("Fifth Word :" + fifthword3 + ", with length :" +fifthWordLength);