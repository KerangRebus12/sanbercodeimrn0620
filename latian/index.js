// 1
console.log("1")
var sayHello = "Hello World!"
console.log(sayHello)
// 2 variabel
console.log("2 - variabel")
var name = "Jonson"
var age = 21
var todayIsFriday = false

console.log(name)
console.log(age)
console.log(todayIsFriday)
// 3 operator
console.log("3 - operator")
var angka = 100
console.log(angka == 100)
console.log(angka == 120)
console.log(angka == "100")
console.log(angka === "100")
console.log(angka === 100)
console.log(angka != "100")
console.log(angka !== "100")
console.log(angka !== 100)
// 4 operator kondisional
console.log("kondisional")
console.log("1OR")
console.log(true || true)
console.log(true || false)
console.log(false || false)
console.log("2AND")
console.log(true && true)
console.log(true && false)
console.log(false && false)
// 5 String properties & method
var sentences = "Javasricpt"
var string2 = " Awesome"
console.log(sentences[0])
console.log(sentences[5])
console.log(sentences.length)
console.log(sentences.charAt(3))
console.log(sentences.concat(string2))
// 6 mengubah tipe data
var int = 12;
var real = 3.45;
var arr = [6,7,8,];

var strInt = String(int);
var strReal = String(real);
var strArr = String(arr);

console.log(strInt);
console.log(strReal);
console.log(strArr);
// kondisional if dll
console.log("1")
// if(true){
//     console.log("Jalankan Code")
// }
var mood = "happy"
if(mood == "happy"){
    console.log("Hari Ini Saya Bahagia")
}
// 2
console.log("latian 2")
// var minimarketStatus = "TUTUP";
// if(minimarketStatus == "BUKA"){
//     console.log("Saya Akan Pergi Belanja")
// }else{
//     console.log("Tutup Bro Ga jadi Belanja")
// }
//3
console.log("latian 3")
// var minimarketStatus = "TUTUP";
// var jamBuka = 5;
// if(minimarketStatus == "BUKA"){
//     console.log("Letsgo Belanja Bulanan")
// }else if(jamBuka <=5){
//     console.log("Tunggu Bro Bentar Lagi Buka")
// }else{
//     console.log("Minimarket Tutup Gara-gara Covid Bos")
// }
// 4
console.log("latian 4")
var minimarketStatus = "BUKA";
var telur = "Abis";
var buah = "Abis";

if(minimarketStatus = "BUKA"){
    console.log("Saya Mau Belanja Telur dan Buah");
    if(telur == "Abis" || buah == "Abis"){
        console.log("Belanja Saya Tidak Lengkap")
    }else if(telur = "Abis"){
        console.log("Yahh Telurnya Abis");
    }else if(buah = "Abis"){
        console.log("Yahh Buahnya Abis");
    }
}else{
    console.log("Minimarket Tutup Cuy")
}
// 5
console.log('latian 5');
var tekanTombol = 3;
switch(tekanTombol){
    case 1:{
        console.log("Nyalakan TV");
        break;
    }
    case 2:{
        console.log("Nyalakan AC");
        break;
    }
    case 3:{
        console.log("Nyalankan Lampu");
        break;
    }
    default:{
        console.log("Tidak Terjadi Apa-apa");
        break;
    }
}