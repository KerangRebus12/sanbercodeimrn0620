// latian looping 1
var flag = 1;
while(flag < 10){
    console.log('iterasi ke - ' +flag);
    flag++;
}
// latian looping 2
var deret = 5;
var jumlah = 0;
while(deret > 0){
    jumlah += deret;
    deret--;
    console.log('Jumlah Saat Ini :' + jumlah);
}
// latian looping 3
// for(var i = 0; i <= 10; i++){
//     console.log('Iterasi Ke-' + i);
// }
// latian looping 4
// var jumlah1 = 0;
// for(var deret1 = 5;deret > 0; deret--){
//     jumlah1 += deret1;
//     console.log('Jumlah Saat Ini :' + jumlah1);
// }
// console.log('Jumlah Terakhir :' + jumlah1);
// latina looping 5
for(var deret1 = 0; deret1 <= 10; deret1 +=2){
    console.log('Interasi dengan Increment counter 2 :' + deret1);
}
console.log('---------------------------------------');
for(var deret2 = 15; deret2 >= 0; deret2-=3){
    console.log('Iterasi dengan Decrement counter 3 :' + deret2);
}