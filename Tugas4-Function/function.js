// Soal 1
console.log('---------------------------------');

function teriak(){
    console.log("Halo Sanbers!");
}
teriak();
// Soal 2
console.log('---------------------------------');

function kalikan(angka1, angka2){
    return angka1 * angka2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Soal 3
console.log('---------------------------------');

function introduce(nama, umur, alamat, hobi){
    return "Nama Saya " + nama + ", Umur Saya " + umur + ", Alamat Saya di " + alamat + ", dan Saya mempunyai Hobi yaitu " + hobi;
}

var name = "Agus";
var age = 30;
var address = "Jalan Malioboro, Yogyakarta";
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);