// Soal 1
var nama = "John";
var peran = "";

if(nama == "" && peran == ""){
    console.log("Mohon Isi Nama Terlebih Dahulu");
}else if(nama == "John" && peran == ""){
    console.log("Halo " + nama + ", Silahkan Pilih Peranmu untuk Memulai Game!");
}else if(nama == "Jane" && peran == "Penyihir"){
    console.log("Selamat Datang di Dunia Werewolf, " +nama);
    console.log("Halo Penyihir "+ nama + ", Kamu Dapat Melihat Siapa Yang Menjadi Werewolf");
}else if(nama == "Jenita" && peran == "Guard"){
    console.log("Selamat Datang di Dunia Werewolf, " +nama);
    console.log("Halo Guard "+ nama + ", Kamu Dapat Melindungi Temanmu dari Serangan Werewolf");
}else if(nama == "Junaedi" && peran == "Werewolf"){
    console.log("Selamat Datang di Dunia Werewolf, " +nama);
    console.log("Halo Werewolf "+ nama + ", Kamu Dapat Memangsa Pemain Lain Setiap Malam");
}else{
    console.log("Kamu Bukan Pemain");
}

// Soal 2
var hari = 21;
var bulan = 1;
var tahun  = 1945;

switch(bulan){
    case 1:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Januari " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 2:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Februari " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 3:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Maret " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 4:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " April " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 5:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Mei " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 6:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Juni " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 7:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Juli " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 8:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Agustus " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 9:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " September " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 10:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Oktober " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 11:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " November " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
    case 12:{
        if((hari >= 1 && hari <= 31) && (tahun >= 1900 && tahun <= 2200)){
            console.log(hari + " Desember " + tahun);
        }else{
            console.log("Format Tanggal Yang Anda Masukan Salah");
        }
        break;
    }
}